First create the folder you want to install the framework in.
Then go inside the folder.

    mkdir mywebsite
    cd mywebsite


To install the framework run:

    curl https://bitbucket.org/aperion/aperion-frame-installer/raw/HEAD/getframe.sh | sh

> NOTE: Likely you will need to generate an ssh key and add it to the repository aperion-frame in order to run the command above
This will create the basic structure of the framework. [How to set up an SSH key.](https://support.atlassian.com/bitbucket-cloud/docs/set-up-an-ssh-key/)

Then run:

    sh install.sh
    
The framework is now ready to be used and extended with modules.

If you want to update the framework just run:

    sh update.sh
    sh install.sh