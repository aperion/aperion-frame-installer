[[ -d config ]] && cp config/* configSAVE
[[ -f index.php ]] && cp index.php index.phpSAVE

command -v ssh || apk add --no-cache openssh-client
git clone https://aperion@bitbucket.org/aperion/aperion-frame.git

find . -name ".keep" -type f -delete
rm -r aperion-frame/.git

cp -r aperion-frame/* .
rm -r aperion-frame/


[[ -d configSAVE ]] && cp configSAVE/* config
[[ -f index.phpSAVE ]] && cp index.phpSAVE index.php
[[ -d configSAVE ]] && rm -r configSAVE
[[ -f index.phpSAVE ]] && rm index.phpSAVEls
