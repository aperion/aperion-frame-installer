
[[ -d config ]] && cp config/* configSAVE
[[ -f index.php ]] && cp index.php index.phpSAVE

git archive --remote=ssh://git@bitbucket.org/aperion/aperion-frame-installer.git --format=zip --output="aperion.zip" main
unzip aperion.zip


find . -name ".keep" -type f -delete
rm aperion.zip
cp -r aperion-aperion-frame-*/* .
rm -r aperion-aperion-frame-*/

[[ -d configSAVE ]] && cp configSAVE/* config
[[ -f index.phpSAVE ]] && cp index.phpSAVE index.php
[[ -d configSAVE ]] && rm -r configSAVE
[[ -f index.phpSAVE ]] && rm index.phpSAVE



cd lib
curl -s https://getcomposer.org/installer | php
php composer.phar update
